// a separate module for the table component
import { NgModule } from '@angular/core';
import { TableComponent } from "./table.component";

@NgModule({
    declarations: [
        TableComponent,
    ],
    exports: [
        TableComponent,
    ]
})
export class TableModule {}