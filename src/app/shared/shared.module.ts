import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from '../shared/components/table/table.module';

const PUBLIC_COMPONENTS: any[] = [];
const PUBLIC_DIRECTIVES: any[] = [];
const PUBLIC_PIPES: any[] = [];
//importing and exporting table module to enable table component in multiple modules
@NgModule({
  declarations: [
    ...PUBLIC_COMPONENTS,
    ...PUBLIC_DIRECTIVES,
    ...PUBLIC_PIPES,
  ],
  imports: [
    CommonModule,
    TableModule,
  ],
  exports: [
    CommonModule,
    TableModule,
    ...PUBLIC_COMPONENTS,
    ...PUBLIC_DIRECTIVES,
    ...PUBLIC_PIPES,
  ],
})
export class SharedModule { }
